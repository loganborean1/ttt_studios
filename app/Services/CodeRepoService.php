<?php

namespace App\Services;


use App\Exceptions\TooManyApiRequestsException;
use App\Repository\GithubRepoRepository;
use App\Requests\RepoFetchRequest;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Http\Request;

/**
 * Class CodeRepoService
 * @package App\Services
 */
class CodeRepoService {


    /**
     * @param Request $request
     * @return array
     */
    public function getRepositories(Request $request) {

        $githubRepoRepository = new GithubRepoRepository();

        $repoFetchRequest = new RepoFetchRequest(
            $request->get('page'),
            $request->get('query'),
            $request->get('sort'),
            $request->get('direction'),
            $request->get('action')
        );

        $repoFetchRequest->validate();

        try {

            if ($repoFetchRequest->isSearch()) {
                $repositories = $githubRepoRepository->searchRepositories($repoFetchRequest);
            } else {
                $repositories = $githubRepoRepository->getRepositories($repoFetchRequest);
            }

        } catch (TooManyApiRequestsException $tooManyApiRequestsException) {

        } catch (GuzzleException $e) {
            dd($e);

        }

        return $repositories;

    }




}

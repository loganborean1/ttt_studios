<?php

namespace App\ExternalApis;


use App\Exceptions\TooManyApiRequestsException;
use App\Requests\RepoFetchRequest;
use GuzzleHttp\Client;

class GithubApi {

    const ORG_NAME = "githubtraining";

    const API_BASE = "https://api.github.com";


    public function __construct() { }


    /**
     * @param $resultsPerPage
     * @param int $page
     * @return array|mixed
     * @throws TooManyApiRequestsException
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getRepositories($resultsPerPage, RepoFetchRequest $fetchRequest) {

        // throws exception
        $this->checkTooManyApiRequests();

        $uri = self::API_BASE . "/orgs/" . self::ORG_NAME . "/repos";

        $client = new Client();

        $res = $client->request('GET', $uri, [
            'query' => [
                'per_page' => $resultsPerPage,
                'page' => $fetchRequest->page,
                'sort' => $fetchRequest->sort,
                'direction' => $fetchRequest->direction,
                'query' => $fetchRequest->direction,
            ]
        ]);

        $decoded = json_decode($res->getBody()->getContents(), true);

        $finalResult = $decoded ? $decoded : [];

        return $finalResult;
    }

    /**
     * @param RepoFetchRequest $fetchRequest
     * @return array
     * @throws TooManyApiRequestsException
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function searchRepository(RepoFetchRequest $fetchRequest) {

        // throws exception
        $this->checkTooManyApiRequests();

        $uri = "/search/repositories";

        $q = "q=$fetchRequest->query+in:name+org:githubtraining";

        $client = new Client(['base_uri' => "https://api.github.com"]);
        $res = $client->request('GET', $uri, [
            'query' => $q
        ]);

        $decoded = json_decode($res->getBody()->getContents(), true);

        $finalResult = $decoded ? $decoded['items'] : [];

        return $finalResult;

    }

    /**
     * @throws TooManyApiRequestsException
     */
    private function checkTooManyApiRequests() {

        if (false) {
            throw new TooManyApiRequestsException();
        }

    }


}

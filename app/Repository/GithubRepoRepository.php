<?php

namespace App\Repository;


use App\ExternalApis\GithubApi;
use App\Requests\RepoFetchRequest;

class GithubRepoRepository {

    const REPOS_CACHED = 10;

    /**
     * @param RepoFetchRequest $searchRequest
     * @return array|mixed
     * @throws \App\Exceptions\TooManyApiRequestsException
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getRepositories(RepoFetchRequest $fetchRequest) {

        if ($fetchRequest->isDefaultSearch()) {
            return $this->getCachedRepos();
        }

        $githubApi = new GithubApi();
        $repositories = $githubApi->getRepositories(self::REPOS_CACHED, $fetchRequest);

        return $repositories;
    }

    /**
     * @param RepoFetchRequest $fetchRequest
     * @return array
     * @throws \App\Exceptions\TooManyApiRequestsException
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function searchRepositories(RepoFetchRequest $fetchRequest) {

        $githubApi = new GithubApi();
        $repositories = $githubApi->searchRepository($fetchRequest);
        return $repositories;
    }


    private function getCachedRepos() {

        /*
         *
         * !if db has data cached  || data is not fresh
         *  cache data
         *  return
         *
         * if data is 'fresh'
         *
         */

        return [];

    }

}

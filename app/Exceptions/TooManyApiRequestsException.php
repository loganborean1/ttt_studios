<?php

namespace App\Exceptions;

use Exception;

class TooManyApiRequestsException extends Exception
{
}

<?php

namespace App\Http\Controllers;


use App\Services\CodeRepoService;
use Illuminate\Http\Request;

/**
 * Class HomeController
 * @package App\Http\Controllers
 */
class HomeController {


    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request) {

        $repoService = new CodeRepoService();
        $repositories = $repoService->getRepositories($request);

        return view("index", compact('repositories'));

    }

}

<?php


namespace App\Requests;


class RepoFetchRequest {


    /*
* sort=created_at
* sort=commit_at
* order=desc
* order=asc
* sort
     *
     */

    public $page;
    public $query;
    public $sort;
    public $direction;

    public $action;

    /**
     * RepoSearchRequest constructor.
     * @param $page - passed the string value from the url. Must convert to int
     * @param $query
     * @param $sort
     * @param $direction
     */
    public function __construct($page, $query, $sort, $direction, $action) {

        $this->page = !empty($page) ? intval($page) : 1;
        $this->query = !empty($query) ? $query : "";
        $this->sort = !empty($sort) ? $sort : "created";
        $this->direction = !empty($direction) ? $direction : 'desc';

        $this->action = !empty($action) ? $action : 'order';
    }


    public function isSearch() {
        return $this->action === 'search';
    }


    public function isDefaultSearch() {

        return false;

        return ($this->action === null || $this->action === 'order') &&
            ($this->page === null || $this->page === 1) &&
            (empty($this->query)) &&
            ($this->sort === null || $this->sort === 'created_at') &&
            ($this->direction === null || $this->direction === 'desc');
    }

    public function validate()
    {
        
    }


}

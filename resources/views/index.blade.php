<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Laravel</title>

    <link rel="stylesheet" href="/styles/styles.css">

</head>

<body>

    <div>

        <div>

            <form name="search_form" method="GET">

                <div>

                    Order By
                    <select name="sort">
                        <option value="created" @if (request('sort') === 'created') selected @endif >Creation Date</option>
                        <option value="pushed" @if (request('sort') === 'pushed') selected @endif > Last Commit Date</option>
                    </select>

                    <select name="direction">
                        <option value="desc" @if (request('direction') === 'desc') selected @endif >Descending</option>
                        <option value="asc" @if (request('direction') === 'asc') selected @endif >Ascending</option>
                    </select>

                    <button type="submit" name="action" value="order">Order</button>

                </div>


                <div>

                    <input @if(request('action') === 'search') value="{{ request('query') }}" @endif type="text" name="query" />
                    <button type="submit" name="action" value="search" >Search</button>

                </div>

            </form>

        </div>


    </div>

    @foreach ($repositories as $repository)

        <div class="repository">

            {{ $repository['name'] }}<br/>
            {{ $repository['full_name'] }}
            {{ $repository['created_at'] }}
            {{ $repository['pushed_at'] }}

        </div>

    @endforeach

    <div>

        <div>Prev</div>

        <div>Next</div>

    </div>


</body>

</html>

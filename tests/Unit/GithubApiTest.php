<?php

namespace Tests\Unit;

use App\ExternalApis\GithubApi;
use PHPUnit\Framework\TestCase;

class GithubApiTest extends TestCase
{

    private $githubApi;

    public function __construct()
    {
        parent::__construct();
        $this->githubApi = new GithubApi();
    }


    /**
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function testApiGet()
    {
        $resultsPerPage = 10;
        $response = $this->githubApi->getRepositories($resultsPerPage, 10);

        $this->assertIsArray($response);
        $this->assertLessThanOrEqual($resultsPerPage, count($response));
    }

    public function testSearch()
    {

        $response = $this->githubApi->searchRepo();
        dd($response);

    }
}

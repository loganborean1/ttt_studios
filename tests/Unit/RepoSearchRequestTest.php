<?php

namespace Tests\Unit;

use App\Requests\RepoFetchRequest;
use PHPUnit\Framework\TestCase;

class RepoSearchRequestTest extends TestCase
{


    public function testTypeIntChange()
    {

        $repoSearchRequest = new RepoFetchRequest("2", null, null, null, null);

        $this->assertEquals(2, $repoSearchRequest->page);

    }
}
